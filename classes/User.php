<?
class User {
    public $user_id = 0;
    public $login = 0;
    public $hash = 0;


    function __construct( $user_id ){
        return $this -> find( $user_id );
    }

    static function auth (){
        global $db;

        if(!$_COOKIE['login'] || !$_COOKIE['hash']){
        	return false;
        }

        $query_text = "SELECT * FROM users WHERE login = ? LIMIT 1";
        $query = $db -> prepare($query_text);
        $query -> bind_param("s", $_COOKIE['login']);
        $query -> execute();
        $result = $query -> get_result();
        $row = $result -> fetch_assoc();

        if( sha1('newhash0099'.$row['login'].$row['hash']) != $_COOKIE['hash'] ){
            return false;
        }else{
            return new User( $row['user_id'] );
        }

    }

    static function login ($login, $password){
        global $db;

        $sha1 = sha1('mypassword123321'.$password);
        $sha1_for_cookie = sha1('newhash0099'.$login.$sha1);

        $query_text = "SELECT * FROM users WHERE login = ? && hash = ? LIMIT 1";
        $query = $db -> prepare($query_text);
        $query -> bind_param("ss", $login, $sha1);
        $query -> execute();
        $result = $query -> get_result();
        $row = $result -> fetch_assoc();

        if($row['user_id']){
            setcookie("login", $login, time() + 86400*365, '/');
            setcookie("hash", $sha1_for_cookie, time() + 86400*365, '/');

            return new User( $row['user_id'] );
        }else{
            return false;
        }
    }

    static function logout (){
        setcookie("login", '', time() + 86400*365, '/');
        setcookie("hash", '', time() + 86400*365, '/');
    }

	// Найти ползователя в бд и загрузить его в данный экземпляр класса
    function find ($user_id){
		global $db;

        if($user_id){
			$query = "SELECT * FROM users WHERE user_id = ? LIMIT 1";
			$stmt = $db -> prepare($query);
			$stmt -> bind_param("i", $user_id);
			$stmt -> execute();
			$result = $stmt -> get_result();
			$row = $result -> fetch_assoc();
			if($row['user_id']){
                $this -> user_id = $row['user_id'];
                $this -> login = $row['login'];
                $this -> hash = $row['hash'];

				return true;
			}else{
                return false;
            }
		}
    }

}
?>
