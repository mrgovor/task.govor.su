<?
class Task {

    private static $str_per_page = 3;

    public $task_id = 0;
    public $user_name = '';
    public $email = '';
    public $task_text = '';
    public $task_status = 0;
    public $edited = 0;


    function __construct( $task_id ){
        return $this -> find( $task_id );
    }

    // Найти задание в бд и загрузить его в данный экземпляр класса
    public function find ($task_id){
		global $db;

        if($task_id){
			$query_text = "SELECT * FROM tasks WHERE task_id = ? LIMIT 1";
			$query = $db -> prepare($query_text);
			$query -> bind_param("i", $task_id);
			$query -> execute();

			$result = $query -> get_result();
			$row = $result -> fetch_assoc();
			if($row['task_id']){
                $this -> task_id = $row['task_id'];
                $this -> user_name = $row['user_name'];
                $this -> email = $row['email'];
                $this -> task_text = $row['task_text'];
                $this -> task_status = $row['task_status'];
                $this -> edited = $row['edited'];

				return true;
			}else{
                return false;
            }
		}
    }

    public function update (){
		global $db;

        $query_text = "UPDATE tasks SET user_name = ?, email = ?, task_text = ?, task_status = ?, edited = ? WHERE task_id = ?";
		$query = $db -> prepare($query_text);
		$query -> bind_param("sssiii",
            $this -> user_name,
            $this -> email,
            $this -> task_text,
            $this -> task_status,
            $this -> edited,
            $this -> task_id
            );
		if( $query -> execute() ){
            return true;
		}else{
            return false;
        }
    }



    public static function task_list( $page = 1, $sort_col = '' ,$sort_type = "ASC" ){
        global $db;

        if(
            in_array( $sort_col , ['user_name','email','task_status'] ) &&
            in_array( $sort_type , ['ASC', 'DESC'] )
        ){
            $order = ' ORDER BY '.$sort_col.' '.$sort_type;
        }


        $shift = ($page - 1) * self::$str_per_page;

        $task_list = [];
        $query_text = "SELECT * FROM tasks $order LIMIT ?,?";
        $query = $db -> prepare($query_text);
        $query -> bind_param("ii", $shift, self::$str_per_page);
        $query -> execute();
        $result = $query -> get_result();
        while( $row = $result -> fetch_assoc() ){
            $task_list[] = $row;
        }
        return $task_list;
    }

    public static function pages_count(){
        global $db;

        $pages_count = 0;

        $query_text = "SELECT count(*) as pages_count FROM tasks";
        $query = $db -> prepare($query_text);
        $query -> execute();
        $result = $query -> get_result();
        $row = $result -> fetch_assoc();

        $pages_count = ceil($row['pages_count'] / self::$str_per_page);

        return $pages_count;
    }

    public static function add_task($user_name, $email, $text){
        global $db;

        if(
            $user_name == '' ||
            $email == '' ||
            $text == ''
        ){
            return false;
        }

        $query_text = "INSERT INTO tasks (user_name, email, task_text)values(?,?,?)";
        $query = $db -> prepare($query_text);
        $query -> bind_param("sss",
            $user_name,
            $email,
            $text
        );
        if( $query -> execute() ){
            return true;
        }else{
            return false;
        }

    }

}
?>
