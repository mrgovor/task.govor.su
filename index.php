<?
require_once 'config.php';
function __autoload($class_name) {
    require_once ('classes/'.$class_name . '.php');
}
$iam = User::auth();

?>

<!DOCTYPE html>
<html lang="en" dir="ltr">
    <head>
        <meta charset="utf-8">
        <title>Тестовое задание Говорухин</title>

        <script src="/js/jquery-3.4.1.min.js" charset="utf-8"></script>
        <script src="/js/slider.js" charset="utf-8"></script>
        <script src="https://kit.fontawesome.com/c027a5afdf.js" crossorigin="anonymous"></script>
        <script src="/js/main.js" charset="utf-8"></script>


        <link rel="stylesheet" href="/css/bootstrap-grid.min.css" type="text/css" />
        <link rel="stylesheet" href="/css/main.css" type="text/css" />

    </head>
    <body>
        <div class="wrapper">

            <div class="center">
                <img src="/govor.png" alt="">
            </div>

            <div class="row">
                <div class="col">
                    <a class="href" open_slider="add_task">Добавить задание</a>
                </div>
                <div class="col right">
                    <?if(!$iam){?>
                    <a class="href" open_slider="auth">Авторизация</a>
                    <?}else{?>
                    Добрый день, <?=$iam -> login?> <a class="logout href">(Выйти)</a>
                    <?}?>
                </div>
            </div>

            <div class="row">
                <div class="col">
                    <div class="add_task hidden mt-3">
                        <form class="add_task__form my_form">
                            <input type="hidden" name="task_id" value="0">
                            <input type="text" name="user_name" placeholder="Имя пользователя"><br />
                            <input type="text" name="email" placeholder="Email"><br />
                            <textarea name="task_text" rows="5" placeholder="Текст задания"></textarea><br />
                            <button type="submit">Добавить задание</button>
                            <div class="my_form__notification"></div>
                        </form>
                    </div>
                </div>
                <div class="col right">
                    <?if(!$iam){?>
                    <div class="auth hidden mt-3">
                        <form class="auth__form my_form">
                            <input type="text" name="login" placeholder="Логин"><br />
                            <input type="password" name="password" placeholder="Пароль"><br />
                            <button type="submit">Войти</button>
                            <div class="my_form__notification"></div>
                        </form>
                    </div>
                    <?}?>
                </div>
            </div>


            <div class="my_table">
                <div class="row center my_table__header">
                    <div class="col href" sort="user_name">
                        <span>Имя пользователя</span>
                    </div>
                    <div class="col href" sort="email">
                        <span>Email</span>
                    </div>
                    <div class="col">Текст задачи</div>
                    <div class="col href" sort="task_status">
                        <span>Статус</span>
                    </div>
                </div>

                <div class="my_table__body"></div>
            </div>

            <div class="pagination center"></div>
        </div>



    </body>
</html>
