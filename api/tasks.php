<?
require_once '../config.php';
function __autoload($class_name) {
    require_once ('../classes/'.$class_name . '.php');
}
$iam = User::auth();


$return = [];


if( $_SERVER['REQUEST_METHOD'] == "GET" ){

    if( !filter_var($_GET['task_id'], FILTER_VALIDATE_INT) ){
        http_response_code(400);
    }else{

        $task = new Task( $_GET['task_id'] );

        $return['task']['task_id'] = $task -> task_id;
        $return['task']['user_name'] = $task -> user_name;
        $return['task']['email'] = $task -> email;
        $return['task']['task_text'] = $task -> task_text;
        $return['task']['task_status'] = $task -> task_status;

        http_response_code(200);

    }


}elseif( $_SERVER['REQUEST_METHOD'] == "POST" ){

    if( $_POST['email'] == '' ){
        $return['error'][] = 'Заполните емеил';
    }elseif( !filter_var($_POST['email'], FILTER_VALIDATE_EMAIL) ){
        $return['error'][] = 'Емеил указан неверно';
    }
    if( $_POST['user_name'] == '' ){
        $return['error'][] = 'Заполните имя пользователя';
    }else{
        $_POST['user_name'] = htmlspecialchars( $_POST['user_name'] );
    }

    if( $_POST['task_text'] == '' ){
        $return['error'][] = 'Заполните текст задания';
    }else{
        $_POST['task_text'] = htmlspecialchars( $_POST['task_text'] );
    }


    if( !count( $return['error'] ) ){
        if(
            Task::add_task( $_POST['user_name'], $_POST['email'], $_POST['task_text'] )
        ){
            http_response_code(201);
        }else{
            http_response_code(400);
        }
    }else{
        http_response_code(400);
    }

}elseif( $_SERVER['REQUEST_METHOD'] == "PUT" ){
    parse_str(file_get_contents('php://input'), $_PUT);

    if(!$iam){
        http_response_code(401);
        $return['error'][] = 'Ошибка авторизации';

    }else{
        if( !filter_var($_PUT['task_id'], FILTER_VALIDATE_INT) ){
            http_response_code(400);
        }else{

            $task = new Task( $_PUT['task_id'] );


            if( isset($_PUT['task_status']) ){

                if( $_PUT['task_status'] ){
                    $task -> task_status = 1;
                }else{
                    $task -> task_status = 0;
                }

            }else{

                if( $_PUT['email'] == '' ){
                    $return['error'][] = 'Заполните емеил';
                }elseif( !filter_var($_PUT['email'], FILTER_VALIDATE_EMAIL) ){
                    $return['error'][] = 'Емеил указан неверно';
                }
                if( $_PUT['user_name'] == '' ){
                    $return['error'][] = 'Заполните имя пользователя';
                }else{
                    $_PUT['user_name'] = htmlspecialchars( $_PUT['user_name'] );
                }

                if( $_PUT['task_text'] == '' ){
                    $return['error'][] = 'Заполните текст задания';
                }else{
                    $_PUT['task_text'] = htmlspecialchars( $_PUT['task_text'] );
                }

                if(
                    $task -> user_name != $_PUT['user_name'] ||
                    $task -> email != $_PUT['email'] ||
                    $task -> task_text != $_PUT['task_text']
                ){
                    $task -> edited = 1;
                }

                $task -> user_name = $_PUT['user_name'];
                $task -> email = $_PUT['email'];
                $task -> task_text = $_PUT['task_text'];

            }
            if( count($return['error']) > 0 ){
                http_response_code(400);
            }else{
                if( $task -> update() ){
                    http_response_code(200);
                }else{
                    http_response_code(400);
                }
            }


        }
    }

}else{
    http_response_code(400);
}

echo json_encode($return, JSON_UNESCAPED_UNICODE);


?>
