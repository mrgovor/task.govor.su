<?
require_once '../config.php';
function __autoload($class_name) {
    require_once ('../classes/'.$class_name . '.php');
}
$iam = User::auth();


$return = [];

if( $_SERVER['REQUEST_METHOD'] == "GET" ){

    if( !filter_var($_GET['page_number'], FILTER_VALIDATE_INT, ['options' => ['min_range' => 1]]) ){
        $_GET['page_number'] = 1;
    }

    if( !in_array( $_GET['sort_col'], ['user_name','email','task_status'] ) ){
        $_GET['sort_col'] = '';
    }

    http_response_code(200);
    $return['task_list'] = Task::task_list( $_GET['page_number'] , $_GET['sort_col'] , $_GET['sort_type'] );
    $return['pages_count'] = Task::pages_count();
    $return['can_update'] = $iam ? 1 : 0;


}else{
    http_response_code(400);
}

echo json_encode($return, JSON_UNESCAPED_UNICODE);


?>
