<?
require_once '../config.php';
function __autoload($class_name) {
    require_once ('../classes/'.$class_name . '.php');
}
$iam = User::auth();

$return = [];


if( $_SERVER['REQUEST_METHOD'] == "GET" ){

    $iam = User::login($_GET['login'], $_GET['password']);
    if( $iam != false ){
        http_response_code(200);
    }else{
        http_response_code(400);
        $return['error'][] = 'Неверный логин или пароль';
    }

}else{
    http_response_code(400);
}

echo json_encode($return, JSON_UNESCAPED_UNICODE);


?>
