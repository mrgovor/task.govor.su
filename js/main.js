let page__opened_page = 1;
let page__sort_col = '';
let page__sort_type = '';

$(document).ready(function() {

    // Добавить новое или изменить задание
    $('.add_task__form').on('submit', function(event) {

        let add_task__form = $(this);
        add_task__form.find('.my_form__notification').html('');

        let task_id = add_task__form.find('[name=task_id]').val();
        let type = '';

        if( task_id == 0 || task_id == '' ){
            type = 'POST';
        }else{
            type = 'PUT';
        }

        $.ajax({
			url: '/api/tasks.php',
			type: type,
			dataType: 'JSON',
			data: $(this).serialize()
		})
		.done(function(data) {
            add_task__form.trigger('reset');
            add_task__form.find('[name=task_id]').val('');
            $('.add_task__form button').html('Дабавить задание');
            $('[reset_form]').remove();


            if(task_id){
                add_task__form.find('.my_form__notification').html('Задание изменено');
            }else{
                add_task__form.find('.my_form__notification').html('Задание добавлено');
            }

            load_task_list(page__opened_page, page__sort_col, page__sort_type);
		})
        .fail(function(data){

            if( !Array.isArray(data.responseJSON.error) ){
                return false;
            }
            data.responseJSON.error.forEach(function(item){
                add_task__form.find('.my_form__notification').append('<div>'+item+'</div>');
            });

        });

        return false;
    });

    // Авторизация
    $('.auth__form').on('submit', function(event) {

        let auth__form = $(this);
        auth__form.find('.my_form__notification').html('');

        $.ajax({
			url: '/api/login.php',
			type: 'GET',
			dataType: 'JSON',
			data: $(this).serialize()
		})
		.done(function(data) {
            location.reload();
		})
        .fail(function(data){

            if( !Array.isArray(data.responseJSON.error) ){
                return false;
            }
            data.responseJSON.error.forEach(function(item){
                auth__form.find('.my_form__notification').append('<div>'+item+'</div>');
            });

        });

        return false;
    });

    // разлогинование
    $('.logout').on('click', function(event) {
        $.ajax({
			url: '/api/logout.php',
			type: 'GET',
			dataType: 'JSON',
			data: ''
		})
		.done(function(data) {
            location.reload();
		})
        .fail(function(data){
            alert( "При выходе с аккаунта произошла ошибка" );
        });
    });

    // Выбрать страницу
    $(document).on('click', '.select_page', function(event) {
        let page_number = $(this).attr('value');
        page__opened_page = page_number;

        load_task_list(page__opened_page, page__sort_col, page__sort_type);

        return false;
    });

    // Выбор сортировки
    $('.my_table__header [sort]').on('click', function(event) {
        let sort_col = $(this).attr('sort');
        let sort_type_now = $(this).attr('sort_type');
        let sort_type = '';

        $('.my_table__header').find('i').remove();

        if( sort_type_now != 'ASC'){
            sort_type = 'ASC';
            $(this).append('<i class="fas fa-caret-up"></i>');
        }else{
            sort_type = 'DESC';
            $(this).append('<i class="fas fa-caret-down"></i>');
        }

        $(this).attr('sort_type',sort_type);

        page__sort_col = sort_col;
        page__sort_type = sort_type;

        load_task_list(page__opened_page, sort_col, sort_type);
    });


    // Обновление статуса
    $(document).on('click', '[set_status]', function(event) {

        let set_status = $(this).attr('set_status');
        let task_id = $(this).attr('task_id');

        let data = {
            task_id: task_id,
            task_status: set_status,
        };

        $.ajax({
			url: '/api/tasks.php',
			type: 'PUT',
			dataType: 'JSON',
			data: data
		})
		.done(function(data) {
            load_task_list(page__opened_page, page__sort_col, page__sort_type);
		})
        .fail(function(data){
            alert('Ошибка изменения статуса');
        });

        return false;
    });


    // Получение задания по id
    $(document).on('click', '[task_edit]', function(event) {
        let task_id = $(this).attr('task_id');

        let data = {
            task_id: task_id,
        };

        $.ajax({
			url: '/api/tasks.php',
			type: 'GET',
			dataType: 'JSON',
			data: data
		})
		.done(function(data) {
            edit_task(data.task);
		})
        .fail(function(data){
            alert('Ошибка загрузки задания');
        });

        return false;
    });

    $(document).on('click', '[reset_form]', function(event) {
        $('.add_task__form').trigger('reset');
        $('.add_task__form').find('[name=task_id]').val('');
        $('.add_task__form button').html('Дабавить задание');

        $('[reset_form]').remove();
    });


});


// Отработка функций при запуске страницы
$(document).ready(function() {
    load_task_list();
});


function load_task_list(page_number = 1, sort_col = '', sort_type = ''){

    $.ajax({
        url: '/api/task_list.php',
        type: 'GET',
        dataType: 'JSON',
        data: 'page_number='+page_number+'&sort_col='+sort_col+'&sort_type='+sort_type
    })
    .done(function(data) {
        $('.select_page').removeClass('action')
        $('.select_page[value='+page_number+']')
            .addClass('action');

        print_tasks(data);
        print_pagination(data.pages_count);
    })
    .fail(function() {
        alert("Ошибка запроса.");
    });
}

// Создание страницы с заданиями из массива полученного по AJAX
function print_tasks( data ){

    if( !Array.isArray(data.task_list) ){
        return false;
    }

    let my_table__html = '';

    data.task_list.forEach(function(item){
        if( item.task_status == 1 ){
            item.task_status_text = 'выполнено';

            if(data.can_update){
                item.task_status_text = '<span class="href" task_id="'+item.task_id+'" set_status="0">'+item.task_status_text+'</span>';
            }
        }else{
            item.task_status_text = 'не выполнено';

            if(data.can_update){
                item.task_status_text = '<span class="href" task_id="'+item.task_id+'" set_status="1">'+item.task_status_text+'</span>';
            }
        }

        if(data.can_update){
            item.task_text = item.task_text+'<br><span class="href" task_id="'+item.task_id+'" task_edit>(Изменить задание)</span>';
        }

        if( item.edited ){
            item.user_name = item.user_name + '<br>(изменено администратором)';
        }


        my_table__html += '<div class="row">';
        my_table__html += '<div class="col">' + item.user_name + '</div>';
        my_table__html += '<div class="col">' + item.email + '</div>';
        my_table__html += '<div class="col">' + item.task_text + '</div>';
        my_table__html += '<div class="col">' + item.task_status_text + '</div>';
        my_table__html += '</div>';
    });

    $('.my_table__body').html( my_table__html );
}

// Создание пагинации из массива полученного по AJAX
function print_pagination( pages_count ){

    let pagination = '';

    for (var i = 1; i <= pages_count; i++) {
        if ( i == page__opened_page ){
            pagination += '<a class="select_page action" value="' + i + '">' + i + '</a> ';
        }else{
            pagination += '<a class="select_page" value="' + i + '">' + i + '</a> ';
        }
    }


    $('.pagination').html( pagination );
}

function edit_task(task){
    $('[open_slider=add_task]').trigger('click');
    $('.add_task__form [name=task_id]').val(task.task_id);
    $('.add_task__form [name=user_name]').val(task.user_name);
    $('.add_task__form [name=email]').val(task.email);
    $('.add_task__form [name=task_text]').val(task.task_text);

    $('.add_task__form button').html('Изменить задание');
    $('.add_task__form').append('<a class="href" reset_form>(Отменить изменение)</a>');
}
