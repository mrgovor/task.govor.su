$(document).ready(function() {

    // Открытие слайдеров
    $('[open_slider]').on('click', function(event) {
        let slider_name = $(this).attr('open_slider');
        $('.'+slider_name).slideDown(300);
    });

});
